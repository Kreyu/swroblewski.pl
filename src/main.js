import Vue from 'vue'
import App from './App'
import router from './router'
import VueParticles from 'vue-particles'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faPhone, faEnvelope, faArrowLeft } from '@fortawesome/free-solid-svg-icons'
import { faGitlab, faTelegramPlane } from '@fortawesome/free-brands-svg-icons'

Vue.use(VueParticles)

library.add(faPhone, faEnvelope, faGitlab, faTelegramPlane, faArrowLeft)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
