import Vue from 'vue'
import VueRouter from 'vue-router'
import Homepage from '@/pages/Homepage'
import NotFound from '@/pages/NotFound'

Vue.use(VueRouter)

export default new VueRouter({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Homepage',
      component: Homepage
    },
    {
      path: '*',
      name: 'NotFound',
      component: NotFound
    }
  ]
})
