import * as captions from '@/assets/captions.json'

export default {

  /**
   * Return random caption string from given group.
   * @param {string} group
   */
  getRandom (group) {
    return captions[group][Math.floor(Math.random() * captions[group].length)]
  }
}
