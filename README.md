<img src="https://gitlab.com/Kreyu/swroblewski.pl/raw/master/static/favicon.png" width="100px" style="display: inline"/>

### [swroblewski.pl](https://swroblewski.pl)  

 Portfolio website with basic contact information and personal projects.

## Installation

```bash
npm install
npm run dev
npm run build
```
